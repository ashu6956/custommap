package com.mapdemo.surendra.serverConnection;


import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

//import org.apache.http.entity.ContentType;
//import org.apache.http.entity.mime.MIME;
//import org.apache.http.entity.mime.MultipartEntity;
//import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by user on 7/7/2015.
 */
//public class MultipartRequest extends Request<String> {
//
//    private static final String FILE_PART_NAME = "image";
//    private final Response.Listener<String> mListener;
//    private final File mFilePart;
//    // private final File mFilePart_invoice;
//    private final Map<String, String> mStringPart;
//    public MultipartEntityBuilder builder = MultipartEntityBuilder.create();
//    private MultipartEntity entity = new MultipartEntity();

//    public MultipartRequest(String url, Response.ErrorListener errorListener,
//                            Response.Listener<String> listener,
//                            Map<String, String> mStringPart, String type) {
////        super(Method.POST, url, errorListener);
//
//        AndyUtils.appLog("Ashutosh", "MultiPartRequestUrl " + url);
//
//
//        mListener = listener;
////        if (mStringPart.get(Const.Params.PICTURE) != null && !mStringPart.get(Const.Params.PICTURE).equals("")) {
////            mFilePart = new File(mStringPart.get(Const.Params.PICTURE));
////        } else if (mStringPart.get(Const.Params.DATA) != null && !mStringPart.get(Const.Params.DATA).equals("")) {
////            mFilePart = new File(mStringPart.get(Const.Params.DATA));
////        } else
////            mFilePart = null;
//        AndyUtils.appLog("MultiPart", "FileType" + type);
//
//
//        if (type.equals(Const.Params.PICTURE)) {
//            mFilePart = new File(mStringPart.get(Const.Params.PICTURE));
//        } else
//            mFilePart = null;
//
//        if (type.equals(Const.Params.PICTURE)) {
//            mStringPart.remove(Const.Params.PICTURE);
//        }
//        this.mStringPart = mStringPart;
//        buildMultipartEntity(type);
//        //}
//    }
//
//    private void buildMultipartEntity(String type) {
//
//        if (mFilePart != null && type.equals(Const.Params.PICTURE)) {
//            builder.addBinaryBody(Const.Params.PICTURE, mFilePart, ContentType.MULTIPART_FORM_DATA, mFilePart.getName());
//        }
//
//        try {
//            for (String key : mStringPart.keySet()) {
//                builder.addTextBody(key, mStringPart.get(key), ContentType
//                        .create("text/plain", MIME.UTF8_CHARSET));
////                builder.addPart(key, new StringBody(mStringPart.get(key)));
//            }
//        } catch (Exception e) {
//            VolleyLog.e("UnsupportedEncodingException");
//        }
//    }
//
////    @Override
////    public String getBodyContentType() {
////        return entity.getContentType().getValue();
////    }
////
////    @Override
////    public byte[] getBody() throws AuthFailureError {
////        ByteArrayOutputStream bos = new ByteArrayOutputStream();
////        try {
////            builder.writeTo(bos);
////        } catch (IOException e) {
////            VolleyLog.e("IOException writing to ByteArrayOutputStream");
////        }
////        return bos.toByteArray();
////    }
//
//
//    @Override
//    protected Response<String> parseNetworkResponse(NetworkResponse response) {
//        String parsed;
//        try {
//            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
//        } catch (UnsupportedEncodingException e) {
//            parsed = new String(response.data);
//        }
//        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
//    }
//
//    @Override
//    protected void deliverResponse(String response) {
//        mListener.onResponse(response);
//    }
//}
