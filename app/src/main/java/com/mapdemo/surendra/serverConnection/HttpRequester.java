package com.mapdemo.surendra.serverConnection;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.mapdemo.surendra.app.AppController;
import com.mapdemo.surendra.custom_interface.ServerApiResponse;
import com.mapdemo.surendra.util.AndyUtils;
import com.mapdemo.surendra.util.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Amal on 28-06-2015.
 */
public class HttpRequester {

    int servicecode;
    private Context activity;
    private ServerApiResponse asyncTaskCompleteListener;
    private Map<String, String> map;


    public HttpRequester(Context activity, int method_type, Map<String, String> map, int servicecode, ServerApiResponse asyncTaskCompleteListener) {
        int method;
        this.activity = activity;
        this.asyncTaskCompleteListener = asyncTaskCompleteListener;
        this.map = map;

        this.servicecode = servicecode;
        if (method_type == 0)
            method = Request.Method.GET;
        else
            method = Request.Method.POST;
        String URL = map.get(Const.Params.URL);
        map.remove(Const.Params.URL);

        if (method == Request.Method.POST)
            volley_requester(method, URL, (map == null) ? null : map);
        else
            volley_requester(URL);


    }


    public HttpRequester(Context activity, int method_type, Map<String, String> map, int servicecode, ServerApiResponse asyncTaskCompleteListener, Map<String, String> headerMap) {
        int method = 0;
        this.activity = activity;
        this.asyncTaskCompleteListener = asyncTaskCompleteListener;
        this.map = map;

        this.servicecode = servicecode;
        if (method_type == 0)
            method = Request.Method.GET;
        else if (method_type == 1)
            method = Request.Method.POST;
        else if (method_type == 2)
            method = Request.Method.PUT;
        else if (method_type == 3)
            method = Request.Method.DELETE;

        String URL = map.get(Const.Params.URL);
        map.remove(Const.Params.URL);

        if (method == Request.Method.POST || method == Request.Method.DELETE || method == Request.Method.PUT)
            volley_requesterHeader(method, URL, (map == null) ? null : map, (headerMap == null) ? null : headerMap);
        else if (method == Request.Method.GET)
            volley_requesterHeader(URL, headerMap);


    }

    public void volley_requester(int method, String url, final Map<String, String> requestbody) {

        AndyUtils.appLog("Ashutosh", "Url in http " + url);

        StringRequest jsonObjRequest = new StringRequest(method,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {
                            AndyUtils.appLog("HttpRequester Response", response.toString());

                            asyncTaskCompleteListener.onTaskCompleted(response.toString(), servicecode);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AndyUtils.appLog("HttpRequester Error", error.toString());
                if (error instanceof NoConnectionError) {
                    Log.d("pavan", "volley requester 1" + error.toString());
                    String msg = "No network connection.Please check your internet";
                }
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                AndyUtils.appLog("HttpRequester", " GetParams");
                Map<String, String> params = new HashMap<String, String>();
                params = requestbody;
                return params;
            }

        };

        jsonObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                Const.TIMEOUT,
                Const.MAX_RETRY,
                Const.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjRequest);

    }

    public void volley_requesterHeader(int method, String url, final Map<String, String> requestbody, final Map<String, String> headerMap) {

        AndyUtils.appLog("Ashutosh", "Url in http " + url);

        StringRequest jsonObjRequest = new StringRequest
                (method,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response != null) {
                                    AndyUtils.appLog("HttpRequester Response", response.toString());

                                    asyncTaskCompleteListener.onTaskCompleted(response.toString(), servicecode);
                                }
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AndyUtils.appLog("HttpRequester Error", error.toString());
                        if (error instanceof NoConnectionError) {
                            Log.d("pavan", "volley requester 1" + error.toString());
                            String msg = "No network connection.Please check your internet";
                        }
                    }
                }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                AndyUtils.appLog("HttpRequester", " GetParams");
                Map<String, String> params = new HashMap<String, String>();
                params = requestbody;
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                if (headerMap != null) {
                    return headerMap;

                }
                return headerMap;
            }
        };

        jsonObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                Const.TIMEOUT,
                Const.MAX_RETRY,
                Const.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjRequest);

    }

    public void volley_requester(String url) {

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject("{}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonGetRequest = new JsonObjectRequest(Request.Method.GET, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {
                            // display response
                            asyncTaskCompleteListener.onTaskCompleted(response.toString(), servicecode);
                            Log.d("Ashutosh", "volley requester response " + response.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Ashutosh", "volley requester 2" + error.toString());
                        String msg = "No network connection.Please check your internet";
                    }
                }
        );
        AppController.getInstance().addToRequestQueue(jsonGetRequest);
    }

    public void volley_requesterHeader(String url, final Map<String, String> headerMap) {

        JsonObjectRequest jsonGetRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {
                            // display response
                            asyncTaskCompleteListener.onTaskCompleted(response.toString(), servicecode);
                            Log.d("Ashutosh", "volley requester response " + response.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Ashutosh", "volley requester 2" + error.toString());
                        String msg = "No network connection.Please check your internet";
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headerMap;
            }
        };


        AppController.getInstance().addToRequestQueue(jsonGetRequest);
    }


}
