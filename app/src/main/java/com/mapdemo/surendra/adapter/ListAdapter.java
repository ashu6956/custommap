package com.mapdemo.surendra.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mapdemo.surendra.R;
import com.mapdemo.surendra.activity.StartEndDateActivity;
import com.mapdemo.surendra.model.DeviceDetails;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ashutosh on 7/22/2018.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.CustomHolder> {

    private Context mContext;
    private List<DeviceDetails> deviceDetailsList;

    public ListAdapter(Context context, List<DeviceDetails> deviceDetailsList) {
        mContext = context;
        this.deviceDetailsList = deviceDetailsList;
    }

    @Override
    public CustomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_list_item, parent, false);
        return new CustomHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomHolder holder, int position) {

        final DeviceDetails deviceDetails = deviceDetailsList.get(position);
        holder.imeiText.setText("IMEI:" + deviceDetails.getImei());
        holder.deviceNameText.setText(deviceDetails.getDeviceName());
        holder.scopeTypeText.setText(deviceDetails.getScopeType());
        holder.trackingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, StartEndDateActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("list", (Serializable) deviceDetailsList);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return deviceDetailsList.size();
    }

    class CustomHolder extends RecyclerView.ViewHolder {

        private LinearLayout trackingLayout;
        private TextView imeiText, deviceNameText, scopeTypeText;


        public CustomHolder(View itemView) {
            super(itemView);
            trackingLayout = itemView.findViewById(R.id.ll_tracking);
            imeiText = itemView.findViewById(R.id.tv_imeiNo);
            deviceNameText = itemView.findViewById(R.id.tv_deviceNo);
            scopeTypeText = itemView.findViewById(R.id.tv_satellites);
        }
    }
}
