package com.mapdemo.surendra.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mapdemo.surendra.R;
import com.mapdemo.surendra.custom_interface.ServerApiResponse;
import com.mapdemo.surendra.model.LocationDetails;
import com.mapdemo.surendra.serverConnection.HttpRequester;
import com.mapdemo.surendra.util.AndyUtils;
import com.mapdemo.surendra.util.Const;
import com.mapdemo.surendra.util.NetworkUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LocationDrawPathActivity extends AppCompatActivity implements ServerApiResponse {

    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private static final String TAG = LocationDrawPathActivity.class.getSimpleName();
    String sIMEI, sEndDateTime, sStartDateTime;
    private List<LocationDetails> locationDetailsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_fence);
        mMapView = findViewById(R.id.geoFenceMapView);
        mMapView.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        sIMEI = bundle.getString("imei");
        sEndDateTime = bundle.getString("end");
        sStartDateTime = bundle.getString("begin");

        getList();

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;
                getList();
            }
        });


    }


    private void getList() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServerURL.HOST_URL);
//        map.put("begin_time", sStartDateTime);
//        map.put("end_time", sEndDateTime);
//        map.put("imei", sIMEI);
//        map.put("map_type", "GOOGLE");
        map.putAll(new NetworkUtils(this).getParamsForTrackingData());
//        map.putAll(NetworkUtils.getParamsForList((String) new PreferenceHelper(this).getParam(this, "token", ""), "jimi.device.track.list"));
//        map.putAll(NetworkUtils.getParamsForList("3e530a384c67a06ebe1cd86a730a6dde", "jimi.device.track.list"));

        AndyUtils.appLog(TAG, "TrackListMap" + map.toString());
        new HttpRequester(this, Const.POST, map, Const.ServerResponseCode.TRACK_LIST, this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode) {
            case Const.ServerResponseCode.TRACK_LIST:
                AndyUtils.appLog(TAG, "TrackListResponse" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("message").equals("success")) {
                        locationDetailsList = new ArrayList<>();
                        JSONArray resultArray = jsonObject.optJSONArray("result");
                        if (null != resultArray && resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.optJSONObject(i);
                                LocationDetails locationDetails = new LocationDetails();
                                locationDetails.setLatitude(resultObject.optDouble("lat"));
                                locationDetails.setLongitude(resultObject.optDouble("lng"));
                                locationDetailsList.add(locationDetails);
                            }
                            drawPath();
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
    }

    private void drawPath() {
        PolylineOptions options = new PolylineOptions().width(8).color(Color.BLACK).geodesic(true);
        int listSize = locationDetailsList.size();
        for (int z = 0; z < listSize; z++) {
            LatLng point = new LatLng(locationDetailsList.get(z).getLatitude(), locationDetailsList.get(z).getLongitude());
            options.add(point);
        }

        if (mGoogleMap != null) {
            Polyline poly_line = mGoogleMap.addPolyline(options);
        }
        LatLng coordinate = new LatLng(locationDetailsList.get(0).getLatitude(), locationDetailsList.get(0).getLongitude());
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                coordinate, 15);
        MarkerOptions currentOption = new MarkerOptions();
        currentOption.position(coordinate);
        currentOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mGoogleMap.addMarker(currentOption);
        mGoogleMap.animateCamera(location);

        if (listSize >= 2) {
            MarkerOptions desOption = new MarkerOptions();
            desOption.position(new LatLng(locationDetailsList.get(listSize - 1).getLatitude(), locationDetailsList.get(listSize - 1).getLongitude()));
            desOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            mGoogleMap.addMarker(desOption);
        }
    }


}
