package com.mapdemo.surendra.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mapdemo.surendra.R;
import com.mapdemo.surendra.model.DeviceDetails;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StartEndDateActivity extends AppCompatActivity implements View.OnClickListener {

    DatePickerDialog dpd;
    TimePickerDialog tpd;
    private LinearLayout startDateTimeLayout, endDateTimeLayout;
    private TextView startDateTime, endDateTime;
    private String mStartDateTime, mEndDateTime;
    private boolean isStartClicked, isEndClicked;
    private Button okButton, cancelButton;
    private String sIMEI;
    private List<DeviceDetails> deviceDetailsList;
    private String[] deviceName;
    private String[] imeiNo;
    private Spinner mSpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_end_date);
        deviceDetailsList = (List<DeviceDetails>) getIntent().getExtras().getSerializable("list");
        mSpinner = findViewById(R.id.sp_deviceList);
        startDateTimeLayout = findViewById(R.id.ll_start_date_and_time);
        endDateTimeLayout = findViewById(R.id.ll_end_date_and_time);
        startDateTime = findViewById(R.id.tv_startDateTime);
        endDateTime = findViewById(R.id.tv_endDateTime);
        okButton = findViewById(R.id.bn_ok);
        cancelButton = findViewById(R.id.bn_cancel);
        okButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        startDateTimeLayout.setOnClickListener(this);
        endDateTimeLayout.setOnClickListener(this);
        getIMEI();
    }

    private void getIMEI() {
        if (null != deviceDetailsList && deviceDetailsList.size() > 0) {
            deviceName = new String[deviceDetailsList.size()];
            imeiNo = new String[deviceDetailsList.size()];
            for (int i = 0; i < deviceDetailsList.size(); i++) {
                deviceName[i] = deviceDetailsList.get(i).getDeviceName();
                imeiNo[i] = deviceDetailsList.get(i).getImei();
            }

            ArrayAdapter<String> deviceAdapter = new ArrayAdapter<String>(this, R.layout.view_spinner_layout, deviceName);
            deviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinner.setAdapter(deviceAdapter);
        }

    }


    private void datePicker() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        dpd = new DatePickerDialog(StartEndDateActivity.this, R.style.datepicker,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(android.widget.DatePicker view,
                                          int year, int monthOfYear, int dayOfMonth) {

                        if (view.isShown()) {
                            if (isStartClicked) {
                                mStartDateTime = Integer.toString(year) + "-"
                                        + Integer.toString(monthOfYear + 1) + "-"
                                        + Integer.toString(dayOfMonth);

                            } else if (isEndClicked) {
                                mEndDateTime = Integer.toString(year) + "-"
                                        + Integer.toString(monthOfYear + 1) + "-"
                                        + Integer.toString(dayOfMonth);
                            }
                            timePicker();
                            dpd.dismiss();
                        }
                    }
                }, mYear, mMonth, mDay);

        dpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dpd.dismiss();
                    }
                });

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 3);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        dpd.getDatePicker().setMaxDate(cal.getTimeInMillis());
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        dpd.getDatePicker().setMaxDate(cal.getTimeInMillis());

        dpd.show();
    }

    public void timePicker() {

        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        tpd = new TimePickerDialog(StartEndDateActivity.this, R.style.datepicker,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(android.widget.TimePicker view,
                                          int hourOfDay, int minute) {
                        if (view.isShown()) {
                            tpd.dismiss();


//
                            if (isStartClicked) {
                                mStartDateTime = mStartDateTime.concat(" "
                                        + Integer.toString(hourOfDay) + ":"
                                        + Integer.toString(minute) + ":" + "00");
                                startDateTime.setText(mStartDateTime);
                            } else if (isEndClicked) {
                                mEndDateTime = mEndDateTime.concat(" "
                                        + Integer.toString(hourOfDay) + ":"
                                        + Integer.toString(minute) + ":" + "00");
                                endDateTime.setText(mEndDateTime);
                            }
                        }
                    }
                }, mHour, mMinute, false);

        tpd.setButton(DialogInterface.BUTTON_NEGATIVE, "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        tpd.show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bn_ok:
                Intent intent = new Intent(this, LocationDrawPathActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("begin", mStartDateTime);
                bundle.putString("end", mEndDateTime);
                bundle.putString("imei", mSpinner.getSelectedItem().toString());
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.bn_cancel:
                onBackPressed();
                break;
            case R.id.ll_start_date_and_time:
                isStartClicked = true;
                isEndClicked = false;
                datePicker();
                break;
            case R.id.ll_end_date_and_time:
                isEndClicked = true;
                isStartClicked = false;
                datePicker();
                break;
        }
    }

}
