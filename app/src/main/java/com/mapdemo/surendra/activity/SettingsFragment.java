package com.mapdemo.surendra.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mapdemo.surendra.R;

/**
 * Created by Ashutosh on 7/25/2018.
 */

public class SettingsFragment extends Fragment {


    private LinearLayout geoFenceLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        geoFenceLayout = view.findViewById(R.id.ll_geoFence);
        geoFenceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CustomGeoFenceActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }
}
