package com.mapdemo.surendra.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.mapdemo.surendra.R;
import com.mapdemo.surendra.custom_interface.ServerApiResponse;
import com.mapdemo.surendra.fragment.ListFragment;
import com.mapdemo.surendra.fragment.MapFragment;
import com.mapdemo.surendra.serverConnection.HttpRequester;
import com.mapdemo.surendra.shared_prefence.PreferenceHelper;
import com.mapdemo.surendra.util.AndyUtils;
import com.mapdemo.surendra.util.Const;
import com.mapdemo.surendra.util.NetworkUtils;
import com.mapdemo.surendra.util.RunTimePermission;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback, ServerApiResponse {

    private TabLayout mTabLayout;
    private TextView tabOne, tabTwo, tabThree, tabFour;
    private static final int PERMISSION_REQUEST = 0;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTabLayout = findViewById(R.id.tabLayout);
        mTabLayout.addTab(mTabLayout.newTab());
        mTabLayout.addTab(mTabLayout.newTab());
        mTabLayout.addTab(mTabLayout.newTab());
        mTabLayout.addTab(mTabLayout.newTab());
        setTabIcon(mTabLayout);
        getAccessToken();

    }

    private void setTabIcon(final TabLayout tabLayout) {
        setupTabIcons();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                AndyUtils.appLog("TabSelected Position", tab.getPosition() + "");
                switch (tab.getPosition()) {
                    case 0:
                        tabOne.setTextColor(getResources().getColor(R.color.tab_color));
                        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home_map_marker, 0, 0);
                        mTabLayout.getTabAt(0).setCustomView(tabOne);
                        addFragment(new MapFragment(), false, "", "", false);
                        break;
                    case 1:
                        tabTwo.setTextColor(getResources().getColor(R.color.tab_color));
                        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.calendar_text, 0, 0);
                        mTabLayout.getTabAt(1).setCustomView(tabTwo);
                        addFragment(new ListFragment(), false, "", "", true);
                        break;
                    case 2:
                        tabThree.setTextColor(getResources().getColor(R.color.tab_color));
                        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.bell, 0, 0);
                        mTabLayout.getTabAt(2).setCustomView(tabThree);
//                        addFragment(new StoreCategoryFragment(), false, "", Const.StoreCategoryFragment, true);
                        break;
                    case 3:
                        tabFour.setTextColor(getResources().getColor(R.color.tab_color));
                        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.account_settings_variant, 0, 0);
                        mTabLayout.getTabAt(3).setCustomView(tabFour);
                        addFragment(new SettingsFragment(), false, "", "", true);
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        tabOne.setTextColor(getResources().getColor(R.color.dark_gray));
                        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home_map_marker_unselected, 0, 0);
                        mTabLayout.getTabAt(0).setCustomView(tabOne);
//                        addFragment(new HomeFragment(), false, "", Const.HOME_FRAGMENT, true);
                        break;
                    case 1:
                        tabTwo.setTextColor(getResources().getColor(R.color.dark_gray));
                        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.calendar_text_unselected, 0, 0);
                        mTabLayout.getTabAt(1).setCustomView(tabTwo);
//                        addFragment(new HealthFeedFragment(), false, "", Const.HealthFeedsFragment, true);
                        break;
                    case 2:
                        tabThree.setTextColor(getResources().getColor(R.color.dark_gray));
                        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.bell_unselected, 0, 0);
                        mTabLayout.getTabAt(2).setCustomView(tabThree);
//                        addFragment(new StoreCategoryFragment(), false, "", Const.StoreCategoryFragment, true);
                        break;
                    case 3:
                        tabFour.setTextColor(getResources().getColor(R.color.dark_gray));
                        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.account_settings_variant_unselected, 0, 0);
                        mTabLayout.getTabAt(3).setCustomView(tabFour);
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        tabOne.setTextColor(getResources().getColor(R.color.tab_color));
                        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home_map_marker, 0, 0);
                        mTabLayout.getTabAt(0).setCustomView(tabOne);
                        addFragment(new MapFragment(), false, "", "", false);
                        break;
                    case 1:
                        tabTwo.setTextColor(getResources().getColor(R.color.tab_color));
                        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.calendar_text, 0, 0);
                        mTabLayout.getTabAt(1).setCustomView(tabTwo);
                        addFragment(new ListFragment(), false, "", "", true);
                        break;
                    case 2:
                        tabThree.setTextColor(getResources().getColor(R.color.tab_color));
                        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.bell, 0, 0);
                        mTabLayout.getTabAt(2).setCustomView(tabThree);
//                        addFragment(new StoreCategoryFragment(), false, "", Const.StoreCategoryFragment, true);
                        break;
                    case 3:
                        tabFour.setTextColor(getResources().getColor(R.color.tab_color));
                        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.account_settings_variant, 0, 0);
                        mTabLayout.getTabAt(3).setCustomView(tabFour);
                        addFragment(new ListFragment(), false, "", "", true);
                        break;
                }
            }
        });
    }


    private void setupTabIcons() {
        tabOne = (TextView) LayoutInflater.from(MainActivity.this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Home");
        tabOne.setTextColor(getResources().getColor(R.color.tab_color));
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home_map_marker, 0, 0);
        mTabLayout.getTabAt(0).setCustomView(tabOne);

        tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("List");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.calendar_text_unselected, 0, 0);
        mTabLayout.getTabAt(1).setCustomView(tabTwo);

        tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("Message");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.bell_unselected, 0, 0);
        mTabLayout.getTabAt(2).setCustomView(tabThree);

        tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText("Profile");
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.account_settings_variant_unselected, 0, 0);
        mTabLayout.getTabAt(3).setCustomView(tabFour);
    }


    @Override
    protected void onResume() {
        super.onResume();
        requestPermissions();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    public void addFragment(Fragment fragment, boolean addToBackStack, String fragmentTitle,
                            String tag, boolean isAnimate) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.contentFrame, fragment, tag);
        ft.commit();
    }


    private void requestPermissions() {
        String[] notGranted = RunTimePermission.CasinoPermissions.getPermissionNotGranted(getApplicationContext());
        if (notGranted != null && notGranted.length > 0) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    notGranted,
                    PERMISSION_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST) {
            // Request for permissions.
            if (RunTimePermission.CasinoPermissions.isAllPermissionGranted(getApplicationContext())) {

//                Intent intent = getIntent();
//                overridePendingTransition(0, 0);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                finish();
//                overridePendingTransition(0, 0);
//                startActivity(intent);
            } else {
                requestPermissions();
            }
        }
    }


    private void getAccessToken() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServerURL.HOST_URL);
        map.putAll(NetworkUtils.getParamsForAccesstoken());

        AndyUtils.appLog(TAG, "AccessTokenMap" + map.toString());
        new HttpRequester(MainActivity.this, Const.POST, map, Const.ServerResponseCode.ACCESSTOKEN, this);


    }


    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode) {
            case Const.ServerResponseCode.ACCESSTOKEN:
                AndyUtils.appLog(TAG, "AccessTokenResponse" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("message").equals("success")) {
                        JSONObject resultObject = jsonObject.optJSONObject("result");
                        if (null != resultObject) {
                            new PreferenceHelper(MainActivity.this).setParam(this, "token", resultObject.optString("accessToken"));
                            addFragment(new MapFragment(), false, "", "", false);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }


}
