package com.mapdemo.surendra.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapdemo.surendra.R;
import com.mapdemo.surendra.adapter.ListAdapter;
import com.mapdemo.surendra.custom_interface.ServerApiResponse;
import com.mapdemo.surendra.model.DeviceDetails;
import com.mapdemo.surendra.serverConnection.HttpRequester;
import com.mapdemo.surendra.util.AndyUtils;
import com.mapdemo.surendra.util.Const;
import com.mapdemo.surendra.util.NetworkUtils;
import com.mapdemo.surendra.util.VerticalSpaceItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ListFragment extends Fragment implements ServerApiResponse {
    private static final String TAG = ListFragment.class.getSimpleName();
    private RecyclerView mRecyclerView;

    // TODO: Rename and change types of parameters
    private List<DeviceDetails> deviceDetailsList;
    private ListAdapter listAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deviceDetailsList = new ArrayList<>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        mRecyclerView = view.findViewById(R.id.rv_list);
        listAdapter = new ListAdapter(getContext(), deviceDetailsList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(20));
        mRecyclerView.setAdapter(listAdapter);
        getList();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode) {
            case Const.ServerResponseCode.LIST:
                AndyUtils.appLog(TAG, "ListResponse" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("message").equals("success")) {
                        JSONArray resultArray = jsonObject.optJSONArray("result");
                        if (null != resultArray && resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.optJSONObject(i);
                                DeviceDetails deviceDetails = new DeviceDetails();
                                deviceDetails.setImei(resultObject.optString("imei"));
                                deviceDetails.setDeviceName(resultObject.optString("deviceName"));
                                deviceDetails.setScopeType(resultObject.optString("mcTypeUseScope"));
                                deviceDetailsList.add(deviceDetails);
                            }
                            listAdapter.notifyDataSetChanged();

                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }

    private void getList() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServerURL.HOST_URL);
        map.putAll(new NetworkUtils(getContext()).getDevicesWithAccount());
//        map.putAll(NetworkUtils.getParamsForList((String) new PreferenceHelper(getContext()).getParam(getContext(), "token", ""), "jimi.user.device.list"));

        AndyUtils.appLog(TAG, "ListMap" + map.toString());
        new HttpRequester(getContext(), Const.POST, map, Const.ServerResponseCode.LIST, this);
    }
}
