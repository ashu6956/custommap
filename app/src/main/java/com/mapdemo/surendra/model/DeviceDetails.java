package com.mapdemo.surendra.model;

import java.io.Serializable;

/**
 * Created by Ashutosh on 7/25/2018.
 */

public class DeviceDetails implements Serializable {

    private String imei;
    private String deviceName;
    private String scopeType;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getScopeType() {
        return scopeType;
    }

    public void setScopeType(String scopeType) {
        this.scopeType = scopeType;
    }
}
