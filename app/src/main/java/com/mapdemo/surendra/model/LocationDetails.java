package com.mapdemo.surendra.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ashutosh on 7/19/2018.
 */

public class LocationDetails implements Serializable {
    @SerializedName("_id")
    private String id;

    @SerializedName("lat")
    private Double latitude;

    @SerializedName("lng")
    private Double longitude;

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @SerializedName("imei")
    private String IMEI;

    @SerializedName("trackerId")
    private String trackerId;

    public String getId() {
        return id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getIMEI() {
        return IMEI;
    }

    public String getTrackerId() {
        return trackerId;
    }

    public String getUTCTime() {
        return UTCTime;
    }

    @SerializedName("hbTime")
    private String UTCTime;


}
