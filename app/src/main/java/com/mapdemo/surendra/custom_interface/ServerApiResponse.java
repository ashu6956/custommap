package com.mapdemo.surendra.custom_interface;

/**
 * Created by user on 8/18/2016.
 */
public interface ServerApiResponse
{
    void onTaskCompleted(String response, int serviceCode);
}
