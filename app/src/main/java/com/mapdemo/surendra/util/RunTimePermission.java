package com.mapdemo.surendra.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Ashutosh on 7/18/2018.
 */

public class RunTimePermission
{
    public static class CasinoPermissions {
        static final String[] locationPermissions = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

//        static final String[] telephonePermissions = new String[]{
//                Manifest.permission.CALL_PHONE};
//
//        static final String[] contactsPermissions;
//
//        static {
//            // see https://developer.android.com/intl/ru/guide/topics/security/permissions.html#perm-groups
//            contactsPermissions = new String[]{
//                    Manifest.permission.GET_ACCOUNTS};
//        }
//
        static final String[] storagePermissions;
//
        static {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                storagePermissions = new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE};
            } else {
                storagePermissions = new String[]{};
            }
        }

        public static List<String> getAllPermissions() {
            List<String> permissions = new ArrayList<>();
            permissions.addAll(Arrays.asList(locationPermissions));
//            permissions.addAll(Arrays.asList(contactsPermissions));
//            permissions.addAll(Arrays.asList(storagePermissions));
//            permissions.addAll(Arrays.asList(telephonePermissions));
            return permissions;
        }


        public static boolean isAllPermissionGranted(Context context) {
            List<String> permissions = null;
            try {
                permissions = Arrays.asList(getPermissionNotGranted(context));
            } catch (Exception ignored) {
            }
            if (permissions == null) {
                return false;
            } else if (permissions.size() == 0) {
                return true;
            } else if (permissions.contains(null)) {
                return false;
            }

            for (String permissionName : permissions) {
                if ((ContextCompat.checkSelfPermission(context, permissionName) != PackageManager.PERMISSION_GRANTED)) {
                    return false;
                }
            }
            return true;
        }

        public static String[] getPermissionNotGranted(Context context) {
            List<String> permissions = CasinoPermissions.getAllPermissions();
            List<String> notGranted = new ArrayList<>();
            if (permissions == null) {
                return null;
            } else if (permissions.size() == 0) {
                return null;
            } else if (permissions.contains(null)) {
                return null;
            }

            for (String permissionName : permissions) {
                if (ContextCompat.checkSelfPermission(context, permissionName) != PackageManager.PERMISSION_GRANTED) {
                    notGranted.add(permissionName);
                }
            }
            return notGranted.toArray(new String[0]);
        }


        public static boolean isPermissionGranted(Context context, String permissionName) {
            PackageManager pm = context.getPackageManager();
            int hasPerm = pm.checkPermission(
                    permissionName,
                    context.getPackageName());
            return hasPerm == PackageManager.PERMISSION_GRANTED;
        }
    }
}
