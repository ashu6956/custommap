package com.mapdemo.surendra.util;

/**
 * Created by Ashutosh on 7/18/2018.
 */

public class Const {

    public static final int TIMEOUT = 35000;
    public static final int MAX_RETRY = 3;
    public static final float DEFAULT_BACKOFF_MULT = 1f;
    public static final String GOOGLE_API_KEY = "AIzaSyAK9Xwehs_JNpiLZNx0bjOs812fKR2EncQ";
    public static final String DIRECTION_API_BASE = "https://maps.googleapis.com/maps/api/directions/json?";
    public static final String ORIGIN = "origin";
    public static final String DESTINATION = "destination";
    public static final String EXTANCTION = "sensor=false&mode=driving&alternatives=true&key=" + Const.GOOGLE_API_KEY;
    public static int GET = 0;
    public static int POST = 1;


    public class Params {
        public static final String ID = "id";
        public static final String URL = "url";
        public static final String ACCESS_TOKEN = "accessToken";
        public static final String REFRESH_TOKEN = "refreshToken";

    }

    public class ServerURL
    {
        public static final String HOST_URL = "http://open.10000track.com/route/rest?";
    }


    public class ServerResponseCode
    {
        public static final int ACCESSTOKEN=1;
        public static final int LOCATION=2;
        public static final int LIST=3;
        public static final int TRACK_LIST=4;
    }

}
