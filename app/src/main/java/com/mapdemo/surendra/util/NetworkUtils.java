package com.mapdemo.surendra.util;

import android.content.Context;

import com.mapdemo.surendra.shared_prefence.PreferenceHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class NetworkUtils {

    //    static String accessToken = "3e530a384c67a06ebe1cd86a730a6dde";
//    static String accessToken = "3e530a384c67a06ebe1cd86a730a6dde";
    private Context mContext;
    static String accessToken;

    public static void main(String[] args) throws Exception {
//		System.out.println(getParamsForAccesstoken());
        HashMap<String, String> params = getDevicesWithAccount();
        System.out.println(params);
    }

    public NetworkUtils(Context context) {
        mContext = context;
        accessToken = (String) new PreferenceHelper(mContext).getParam(mContext, "token", "");
    }

//
//    static String accessToken = new PreferenceHelper().getParam();


    public static HashMap<String, String> getParamsForCreatingGeoFence() {

        HashMap<String, String> specificParams = new HashMap();
        specificParams.put("access_token", accessToken);//access token should be retrevied from shared preference manager
        specificParams.put("imei", "358739052105112");
        specificParams.put("fence_name", "SurendraTestFence");
        specificParams.put("alarm_type", "in,out");
        specificParams.put("report_mode", "1");
        specificParams.put("alarm_switch", "ON");
        specificParams.put("lng", "12.9263625");
        specificParams.put("lat", "77.5589076");
        specificParams.put("radius", "1000");
        specificParams.put("zoom_level", "5");
        specificParams.put("map_type", "GOOGLE");

        HashMap<String, String> commonParams = getParameterForSignature("jimi.open.device.fence.create");
        commonParams.put("sign", Signature.getSignature(commonParams, specificParams));
        return commonParams;
    }


    public static HashMap<String, String> getParamsForAccesstoken() {

        HashMap<String, String> specificParams = new HashMap();

        specificParams.put("user_id", "Intugine");

        try {
            specificParams.put("user_pwd_md5", encrptMD5Hex("intugine123"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        specificParams.put("expires_in", "7200");

        HashMap<String, String> commonParams = getParameterForSignature("jimi.oauth.token.get");
        commonParams.put("sign", Signature.getSignature(commonParams, specificParams));
        return commonParams;
    }

    public static HashMap<String, String> getDevicesWithAccount() {

        HashMap<String, String> specificParams = new HashMap();
        specificParams.put("access_token", accessToken);//access token should be retrevied from shared preference manager
        specificParams.put("target", "Intugine");

        HashMap<String, String> commonParams = getParameterForSignature("jimi.user.device.list");
        commonParams.put("sign", Signature.getSignature(commonParams, specificParams));
        return commonParams;
    }


    public static HashMap<String, String> getParamsForTrackingData() {

        HashMap<String, String> specificParams = new HashMap();
        specificParams.put("access_token", accessToken);//access token should be retrevied from shared preference manager
        specificParams.put("imei", "355087090042316");
        specificParams.put("map_type", "GOOGLE");
        specificParams.put("begin_time", "2018-07-19 18:46:11");
        specificParams.put("end_time", "2018-07-21 18:46:11");


        HashMap<String, String> commonParams = getParameterForSignature("jimi.device.track.list");
        commonParams.put("sign", Signature.getSignature(commonParams, specificParams));
        return commonParams;
    }

    public static HashMap<String, String> getParamsForCurrentLocation() {

        HashMap<String, String> specificParams = new HashMap();
        specificParams.put("access_token", accessToken);//access token should be retrevied from shared preference manager
        specificParams.put("imeis", "355087090042316");
        specificParams.put("map_type", "GOOGLE");

        HashMap<String, String> commonParams = getParameterForSignature("jimi.oauth.token.get");
        commonParams.put("sign", Signature.getSignature(commonParams, specificParams));
        return commonParams;
    }

    public HashMap<String, String> getCommonParameterForGps(
            String method, HashMap<String, String> specificParams) {
        HashMap<String, String> commonParams = getParameterForSignature(method);
        commonParams.put("sign", Signature.getSignature(commonParams, specificParams));
        return commonParams;
    }


    public static HashMap<String, String> getParameterForSignature(
            String method) {
        final Date currentTime = new Date();

        final SimpleDateFormat sdf =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Give it to me in GMT time.
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        System.out.println(sdf.format(currentTime));
        try {
            System.out.println(encrptMD5Hex("intugine123"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("method", method);
        params.put("timestamp", sdf.format(currentTime));
//        params.put("timestamp", "2018-07-25 17:01:06");
        params.put("app_key", "8FB345B8693CCD004964E2C6B35629E2");

        params.put("sign_method", "md5");
        params.put("v", "1.0");
        params.put("format", "json");

        return params;
    }

    public static String encrptMD5Hex(String data) throws Exception {

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        md.update(data.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        System.out.println("Digest(in hex format):: " + sb.toString());

        return sb.toString();
    }

}
