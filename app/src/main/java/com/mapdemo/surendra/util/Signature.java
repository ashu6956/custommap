package com.mapdemo.surendra.util;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;



public class Signature {
	private final static String SIGN_METHOD_MD5 = "md5";
	private final static String CHARSET_UTF8 = "UTF-8";

		
	public static String getSignature(HashMap<String,String> paramsForSignature,
			HashMap<String,String> specificParams) {
		final Date currentTime = new Date();
		paramsForSignature.putAll(specificParams);
		String signature="";
		try {
			signature = signTopRequest(paramsForSignature,"2e20c190e8ad406ba789f85174554513","md5");
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(signature);
		return signature;
		
	}

	public static String signTopRequest(HashMap<String, String> params, String seccode, String signMethod)
			throws IOException {
		// 1:sort parameter key
		String[] keys = params.keySet().toArray(new String[0]);
		Arrays.sort(keys);
		System.out.println(keys.toString());
		// 2:: Put all parameter names and parameter values together
		StringBuilder query = new StringBuilder();
		if (SIGN_METHOD_MD5.equals(signMethod)) {
			query.append(seccode);
		}
		for (String key : keys) {
			String value = params.get(key);
			if (StringUtils.isNotEmpty(key) && StringUtils.isNotBlank(value)) {
				query.append(key).append(value);
			}
		}
		
	
		
		// 3: use MD5/HMAC to encrypt
		byte[] bytes;
		query.append(seccode);
		
		System.out.println(seccode);
		
		
		bytes = encryptMD5(query.toString());

		
		System.out.println(bytes);
		// 4: convert binary to uppercase hexadecimal
		String finalRes= byte2hex(bytes);
		System.out.println(finalRes);
		return finalRes;
	}

	public static String encrptMD5Hex(String data) throws Exception{

        MessageDigest md=null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        md.update(data.getBytes());
        
        byte byteData[] = md.digest();
 
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
     
        System.out.println("Digest(in hex format):: " + sb.toString());
        
        return sb.toString();
	}
	public static byte[] encryptMD5(String data) throws IOException {


		byte[] bytesOfMessage = data.getBytes("UTF-8");
		
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] thedigest = md.digest(bytesOfMessage);
		
		return thedigest;
		

        
	}

	public static String byte2hex(byte[] bytes) {
		StringBuilder sign = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(bytes[i] & 0xFF);
			if (hex.length() == 1) {
				sign.append("0");
			}
			sign.append(hex.toUpperCase());
		}
		return sign.toString();
	}
}
















