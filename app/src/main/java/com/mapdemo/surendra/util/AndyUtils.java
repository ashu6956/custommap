package com.mapdemo.surendra.util;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;



/**
 * Created by getit on 8/5/2016.
 */
public class AndyUtils {
    public static Dialog mDialog;
    public static ProgressDialog mProgressDialog;

    public static void appLog(String msg1, String msg2) {
        Log.d(msg1, msg2);
    }

    public static void showLongToast(String msg, Context context) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void showShortToast(String msg, Context context) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


}
