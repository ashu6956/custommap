package com.mapdemo.surendra.shared_prefence;

import android.content.Context;
import android.content.SharedPreferences;

import com.mapdemo.surendra.util.Const;

/**
 * Created by Ashutosh on 7/24/2018.
 */

public class PreferenceHelper
{
    private SharedPreferences app_prefs;
    private Context context;
    public static String PREF_NAME = "SURENDRA_PREFERENCE";
    private static final String FILE_NAME = "SURENDRA";



//    public static PreferenceHelper getInstance()
//    {
//        if(pHelper==null)
//        {
//            pHelper=new PreferenceHelper()
//        }
//
//        return pHelper;
//    }

    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        this.context = context;
    }

    public static void setParam(Context context, String key, Object object) {

        String type = object.getClass().getSimpleName();
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        if ("String".equals(type)) {
            editor.putString(key, (String) object);
        } else if ("Integer".equals(type)) {
            editor.putInt(key, (Integer) object);
        } else if ("Boolean".equals(type)) {
            editor.putBoolean(key, (Boolean) object);
        } else if ("Float".equals(type)) {
            editor.putFloat(key, (Float) object);
        } else if ("Long".equals(type)) {
            editor.putLong(key, (Long) object);
        }

        editor.commit();
    }

    /**
     * @param context
     * @param key
     * @param defaultObject
     * @return
     */
    public static Object getParam(Context context, String key,
                                  Object defaultObject) {
        String type = defaultObject.getClass().getSimpleName();
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);

        if ("String".equals(type)) {
            return sp.getString(key, (String) defaultObject);
        } else if ("Integer".equals(type)) {
            return sp.getInt(key, (Integer) defaultObject);
        } else if ("Boolean".equals(type)) {
            return sp.getBoolean(key, (Boolean) defaultObject);
        } else if ("Float".equals(type)) {
            return sp.getFloat(key, (Float) defaultObject);
        } else if ("Long".equals(type)) {
            return sp.getLong(key, (Long) defaultObject);
        }
        return null;
    }

}
